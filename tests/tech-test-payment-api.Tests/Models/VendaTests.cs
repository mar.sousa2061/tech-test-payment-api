using tech_test_payment_api.Domain.Enums;
using tech_test_payment_api.Domain.Models;
using tech_test_payment_api.Domain.Models.Validations;

namespace tech_test_payment_api.Tests.Models
{
    public class VendaTests
    {
        [Fact]
        public void ValidationDeveRetornoFalsoQuandoPedidoNaoTiverPeloMenosUmItem()
        {
            var venda = new Venda()
            {
                Data = DateTime.Now,
                StatusId = (int)StatusVendaEnum.AguardandoPagamento,
                Status = new StatusVenda()
                {
                    Id = (int)StatusVendaEnum.AguardandoPagamento,
                    Descricao = "Aguardando Pagamento"
                },
                Vendedor = new Vendedor()
                {
                    CPF = "76700251079",
                    Nome = "John Doe",
                    Email = "admin@fake.com",
                    Telefone = new Telefone()
                    {
                        DDD = 12,
                        Numero = 985467323
                    }
                },
                Pedido = new Pedido()
                {
                    Itens = new List<PedidoItem>()
                }
            };

            var retorno = new VendaValidation().Validate(venda);

            Assert.False(retorno.IsValid);
            Assert.Contains("O pedido deve conter no m�nimo 1 item", retorno.Errors.First().ErrorMessage);
        }

        [Fact]
        public void ValidationDeveRetornoVerdadeiroQuandoPassadoVendaValida()
        {
            var venda = new Venda()
            {
                Data = DateTime.Now,
                StatusId = (int)StatusVendaEnum.AguardandoPagamento,
                Status = new StatusVenda()
                {
                    Id = (int)StatusVendaEnum.AguardandoPagamento,
                    Descricao = "Aguardando Pagamento"
                },
                Vendedor = new Vendedor()
                {
                    CPF = "76700251079",
                    Nome = "John Doe",
                    Email = "admin@fake.com",
                    Telefone = new Telefone()
                    {
                        DDD = 12,
                        Numero = 985467323
                    }
                },
                Pedido = new Pedido()
                {
                    Itens = new List<PedidoItem>()
                    {
                        new PedidoItem()
                        {
                            Produto = "Camisa Polo Tam M",
                            Quantidade = 1
                        },
                        new PedidoItem()
                        {
                            Produto = "Cal�a Jeans Tam 40",
                            Quantidade = 1
                        }
                    }
                }
            };

            var retorno = new VendaValidation().Validate(venda);

            Assert.True(retorno.IsValid);
        }
    }
}