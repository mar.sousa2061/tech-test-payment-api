using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Tests.Models
{
    public class VendedorTests
    {
        [Theory]
        [InlineData("12345678910")]
        [InlineData("23421232323")]
        [InlineData("23322456728")]
        [InlineData("53469783543")]
        [InlineData("13274323234")]
        [InlineData("2347823723764")]
        [InlineData("23476233248243")]
        [InlineData("2378964237467246327423")]
        public void ValidarCPFDeveRetornarFalsoQuandoPassadoUmCPFInvalido(string cpf)
        {
            var vendedor = new Vendedor()
            {
                CPF = cpf
            };

            var retorno = vendedor.ValidarCPF();

            Assert.False(retorno);
        }

        [Theory]
        [InlineData("87951953008")]
        [InlineData("76700251079")]
        [InlineData("42358774022")]
        [InlineData("81746129042")]
        [InlineData("20610605011")]
        [InlineData("115.671.710-87")]
        [InlineData("436.076.710-28")]
        [InlineData("293.448.560-95")]
        public void ValidarCPFDeveRetornarVerdadeiroQuandoPassadoUmCPFValido(string cpf)
        {
            var vendedor = new Vendedor()
            {
                CPF = cpf
            };

            var retorno = vendedor.ValidarCPF();

            Assert.True(retorno);
        }

        [Theory]
        [InlineData("edfguhj34t83t34t43")]
        [InlineData("teste@")]
        [InlineData("@teste.com")]
        [InlineData("fake@teste")]
        public void ValidarEmailDeveRetornarFalsoQuandoPassadoUmEmailInvalido(string email)
        {
            var vendedor = new Vendedor()
            {
                Email = email
            };

            var retorno = vendedor.ValidarEmail();

            Assert.False(retorno);
        }

        [Theory]
        [InlineData("admin@fake.com")]
        [InlineData("teste@fake.com")]
        [InlineData("marketing@fake.com")]
        [InlineData("contact@fake.com")]
        public void ValidarEmailDeveRetornarVerdadeiroQuandoPassadoUmEmailValido(string email)
        {
            var vendedor = new Vendedor()
            {
                Email = email
            };

            var retorno = vendedor.ValidarEmail();

            Assert.True(retorno);
        }
    }
}