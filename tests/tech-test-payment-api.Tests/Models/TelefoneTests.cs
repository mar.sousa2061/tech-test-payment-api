using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Tests.Models
{
    public class TelefoneTests
    {
        [Theory]
        [InlineData(000000000)]
        [InlineData(111111111)]
        [InlineData(222222222)]
        [InlineData(333333333)]
        [InlineData(444444444)]
        [InlineData(555555555)]
        [InlineData(666666666)]
        [InlineData(777777777)]
        [InlineData(888888888)]
        [InlineData(999999999)]
        [InlineData(012345678)]
        [InlineData(123456789)]
        [InlineData(876543210)]
        [InlineData(987654321)]
        public void NumeroValidoDeveRetornarFalsoQuandoPassadoUmValorInvalido(int numero)
        {
            var telefone = new Telefone()
            {
                DDD = 12,
                Numero = numero
            };

            var retorno = telefone.NumeroValido();

            Assert.False(retorno);
        }

        [Theory]
        [InlineData(997685436)]
        [InlineData(967583451)]
        [InlineData(957457345)]
        [InlineData(923456789)]
        [InlineData(976543210)]
        [InlineData(934967323)]
        public void NumeroValidoDeveRetornarVerdadeiroQuandoPassadoUmValorValido(int numero)
        {
            var telefone = new Telefone()
            {
                DDD = 12,
                Numero = numero
            };

            var retorno = telefone.NumeroValido();

            Assert.True(retorno);
        }
    }
}