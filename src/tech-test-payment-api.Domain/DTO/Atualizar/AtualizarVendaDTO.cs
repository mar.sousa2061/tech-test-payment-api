﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class AtualizarVendaDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Range(1, 5, ErrorMessage = "O campo {0} deve aceita somente valores entre {1} e {2}")]
        public int StatusId { get; set; }
    }
}
