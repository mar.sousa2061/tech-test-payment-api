﻿namespace tech_test_payment_api.Domain.DTO
{
    public class PedidoItemDTO
    {
        public Guid Id { get; set; }
        public string Produto { get; set; }
        public int Quantidade { get; set; }
    }
}
