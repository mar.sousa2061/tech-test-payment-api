﻿namespace tech_test_payment_api.Domain.DTO
{
    public class VendaDTO
    {
        public Guid Id { get; set; }
        public DateTime Data { get; set; }

        public StatusVendaDTO Status { get; set; }
        public VendedorDTO Vendedor { get; set; }
        public PedidoDTO Pedido { get; set; }
    }
}
