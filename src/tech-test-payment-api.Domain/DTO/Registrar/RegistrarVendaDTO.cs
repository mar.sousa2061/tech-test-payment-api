﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class RegistrarVendaDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public DateTime Data { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public RegistrarVendedorDTO Vendedor { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public RegistrarPedidoDTO Pedido { get; set; }
    }
}
