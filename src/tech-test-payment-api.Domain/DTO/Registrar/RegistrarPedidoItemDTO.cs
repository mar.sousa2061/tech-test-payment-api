﻿using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Domain.DTO
{
    public class RegistrarPedidoItemDTO
    {
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(50, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 3)]
        public string Produto { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "O campo {0} deve ser entre {1} e {2} dígitos")]
        public int Quantidade { get; set; }
    }
}
