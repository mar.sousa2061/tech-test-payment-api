﻿namespace tech_test_payment_api.Domain.Enums
{
    public enum StatusVendaEnum
    {
        AguardandoPagamento = 1,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}
