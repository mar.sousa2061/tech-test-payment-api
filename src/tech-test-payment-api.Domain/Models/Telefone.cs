﻿namespace tech_test_payment_api.Domain.Models
{
    public class Telefone : Entidade
    {
        public int DDD { get; set; }
        public int Numero { get; set; }

        public bool NumeroValido()
        {
            if (Numero.Equals(000000000) ||
                Numero.Equals(111111111) ||
                Numero.Equals(222222222) ||
                Numero.Equals(333333333) ||
                Numero.Equals(444444444) ||
                Numero.Equals(555555555) ||
                Numero.Equals(666666666) ||
                Numero.Equals(777777777) ||
                Numero.Equals(888888888) ||
                Numero.Equals(999999999) ||
                Numero.Equals(012345678) ||
                Numero.Equals(123456789) ||
                Numero.Equals(876543210) ||
                Numero.Equals(987654321)) return false;

            return true;
        }
    }
}
