﻿namespace tech_test_payment_api.Domain.Models
{
    public class Pedido : Entidade
    {
        public List<PedidoItem> Itens { get; set; }
    }
}
