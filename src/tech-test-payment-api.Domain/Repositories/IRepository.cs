﻿using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Domain.Repositories
{
    public interface IRepository<TEntidade> : IDisposable where TEntidade : Entidade
    {
        Task Adicionar(TEntidade entidade);
        Task<TEntidade> ObterPorId(Guid id);
        Task Atualizar(TEntidade entidade);
        Task<int> SaveChanges();
    }
}
