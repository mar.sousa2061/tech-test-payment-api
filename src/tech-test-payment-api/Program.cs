using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;
using tech_test_payment_api.Domain.Notifications;
using tech_test_payment_api.Domain.Repositories;
using tech_test_payment_api.Domain.Services;
using tech_test_payment_api.Infra.Context;
using tech_test_payment_api.Infra.Repositories;
using tech_test_payment_api.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseInMemoryDatabase("tech_test_payment_api"));

builder.Services.AddAutoMapper(typeof(Program));

builder.Services.AddScoped<ApplicationDbContext>();
builder.Services.AddScoped<IVendaRepository, VendaRepository>();
builder.Services.AddScoped<IVendaService, VendaService>();
builder.Services.AddScoped<INotificador, Notificador>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "tech-test-payment-api",
        Description = "Sistema do teste t�cnico para Pottencial =)",
        Contact = new OpenApiContact() { Name = "Marcelo", Email = "mar.sousa2061@gmail.com" }
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "Teste t�cnico Pottencial v1");
        options.RoutePrefix = "api-docs";
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
