﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Domain.Notifications;

namespace tech_test_payment_api.Controllers
{
    public class MainController : ControllerBase
    {
        private readonly INotificador _notificador;

        public MainController(INotificador notificador)
        {
            _notificador = notificador;
        }

        protected bool OperacaoValida()
        {
            return !_notificador.TemNotificacao();
        }

        protected ActionResult CustomResponse()
        {
            return BadRequest(new Dictionary<string, List<Notificacao>>
            {
                { "Notificações", _notificador.ObterNotificacoes() }
            });
        }
    }
}
