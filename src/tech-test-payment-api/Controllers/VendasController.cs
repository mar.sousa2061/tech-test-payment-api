using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Domain.DTO;
using tech_test_payment_api.Domain.Models;
using tech_test_payment_api.Domain.Notifications;
using tech_test_payment_api.Domain.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("v1/vendas")]
    public class VendasController : MainController
    {
        private readonly IVendaService _service;
        private readonly INotificador _notificador;

        public VendasController(IVendaService service, INotificador notificador) : base (notificador)
        {
            _service = service;
        }

        /// <summary>
        /// Resgistrar venda
        /// </summary>
        /// <param name="vendaDTO"></param>
        /// <returns></returns>
        [HttpPost()]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Dictionary<string, List<Notificacao>>))]
        public async Task<IActionResult> Post([FromBody] RegistrarVendaDTO vendaDTO)
        {
            var venda = await _service.RegistarVenda(vendaDTO);

            if (!OperacaoValida()) return CustomResponse();

            return CreatedAtAction(nameof(Get), new { id = venda.Id }, venda);
        }

        /// <summary>
        /// Buscar venda
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(VendaDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Dictionary<string, List<Notificacao>>))]
        public async Task<IActionResult> Get([FromRoute] Guid id)
        {
            var venda = await _service.BuscarVenda(id);

            if (!OperacaoValida()) return CustomResponse();

            return Ok(venda);
        }

        /// <summary>
        /// Atualizar venda
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vendaDTO"></param>
        /// <returns></returns>
        /// <remarks>
        /// Status possíveis: 
        ///     Pagamento Aprovado = 2, 
        ///     Enviado Para Transportadora = 3, 
        ///     Entregue = 4, 
        ///     Cancelada = 5
        /// </remarks>
        [HttpPut("{id:guid}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(Dictionary<string, List<Notificacao>>))]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] AtualizarVendaDTO vendaDTO)
        {
            await _service.AtualizarVenda(id, vendaDTO);

            if (!OperacaoValida()) return CustomResponse();

            return NoContent();
        }
    }
}