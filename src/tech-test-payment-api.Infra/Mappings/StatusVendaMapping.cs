﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Infra.Mappings
{
    public class StatusVendaMapping : IEntityTypeConfiguration<StatusVenda>
    {
        public void Configure(EntityTypeBuilder<StatusVenda> builder)
        {
            builder.HasKey(sv => sv.Id);

            builder.Property(sv => sv.Descricao).IsRequired().HasMaxLength(50);

            builder.HasMany(sv => sv.Vendas)
                .WithOne(v => v.Status)
                .HasForeignKey(v => v.StatusId);            
        }
    }
}
