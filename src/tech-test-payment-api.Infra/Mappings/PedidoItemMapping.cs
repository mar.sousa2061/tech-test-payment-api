﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Infra.Mappings
{
    public class PedidoItemMapping : IEntityTypeConfiguration<PedidoItem>
    {
        public void Configure(EntityTypeBuilder<PedidoItem> builder)
        {
            builder.HasKey(pi => pi.Id);

            builder.Property(pi => pi.Produto).IsRequired().HasMaxLength(50);
            builder.Property(pi => pi.Quantidade).IsRequired();
        }
    }
}
