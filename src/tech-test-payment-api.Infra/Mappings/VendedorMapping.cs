﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using tech_test_payment_api.Domain.Models;

namespace tech_test_payment_api.Infra.Mappings
{
    public class VendedorMapping : IEntityTypeConfiguration<Vendedor>
    {
        public void Configure(EntityTypeBuilder<Vendedor> builder)
        {
            builder.HasKey(v => v.Id);

            builder.Property(v => v.CPF).IsRequired().HasMaxLength(11);
            builder.Property(v => v.Nome).IsRequired().HasMaxLength(100);
            builder.Property(v => v.Email).IsRequired().HasMaxLength(50);

            builder.HasOne(v => v.Telefone);

            builder.HasMany(v => v.Vendas)
                .WithOne(venda => venda.Vendedor)
                .HasForeignKey(venda => venda.VendedorId);
        }
    }
}
