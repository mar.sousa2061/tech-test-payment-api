# Documentação do Projeto

## Desenvolvimento

O projeto foi desenvolvido com base no diagrama abaixo: 😄

<img src="assets/diagrama-projeto.PNG">

## Evidência de funcionamento

Segue abaixo o link do vídeo mostrando a aplicação em funcionamento:

[Vídeo](https://youtu.be/8N3JAa2mDwo)